#!/bin/bash

num_input_files=5
num_map_workers=5
num_reduce_workers=5
input_files_dir=./sample_input_files
output_file=./output/2.output
map_reduce_task_id=2
num_mpi_processes=$((num_reduce_workers + num_map_workers + 1))

mpirun --oversubscribe -np ${num_mpi_processes} ./a03 ${input_files_dir} ${num_input_files} ${num_map_workers} ${num_reduce_workers} ${output_file} ${map_reduce_task_id}
sort ${output_file} | diff sample_output_files/2.output -