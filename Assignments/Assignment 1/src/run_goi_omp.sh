num_threads=$1
log_file=goi_omp_${num_threads}t
out_file=goi_omp

echo -e "goi_omp timing (${num_threads} threads)\n" > ./timing_logs/${log_file}.log

cat /proc/cpuinfo | grep 'vendor' | uniq >> ./timing_logs/${log_file}.log
cat /proc/cpuinfo | grep 'model name' | uniq >> ./timing_logs/${log_file}.log
cat /proc/cpuinfo | grep processor | wc -l >> ./timing_logs/${log_file}.log

echo -e "\nsample0.in" >> ./timing_logs/${log_file}.log
{ time ./goi_omp ./sample_inputs/sample0.in ./outputs/${out_file}0.out ${num_threads}; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample1.in" >> ./timing_logs/${log_file}.log
{ time ./goi_omp ./sample_inputs/sample1.in ./outputs/${out_file}1.out ${num_threads}; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample2.in" >> ./timing_logs/${log_file}.log
{ time ./goi_omp ./sample_inputs/sample2.in ./outputs/${out_file}2.out ${num_threads}; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample3.in" >> ./timing_logs/${log_file}.log
{ time ./goi_omp ./sample_inputs/sample3.in ./outputs/${out_file}3.out ${num_threads}; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample4.in" >> ./timing_logs/${log_file}.log
{ time ./goi_omp ./sample_inputs/sample4.in ./outputs/${out_file}4.out ${num_threads}; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample5.in" >> ./timing_logs/${log_file}.log
{ time ./goi_omp ./sample_inputs/sample5.in ./outputs/${out_file}5.out ${num_threads}; } 2>> ./timing_logs/${log_file}.log

echo -e "\nsample6.in" >> ./timing_logs/${log_file}.log
{ time ./goi_omp ./sample_inputs/sample6.in ./outputs/${out_file}6.out ${num_threads}; } 2>> ./timing_logs/${log_file}.log