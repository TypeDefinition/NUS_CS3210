#!/bin/bash

# Source & Destination Folders
nusnetid=E0544466
studentid=A0218430N
src=src

# Create Temporary Folders
mkdir ./temp
mkdir ./temp/sb

# Copy Report
cp ./${studentid}_report.pdf ./temp/${studentid}_report.pdf

# Copy GOI
cp ./${src}/goi.h ./temp/goi.h
cp ./${src}/goi.c ./temp/goi.c
cp ./${src}/goi_threads.c ./temp/goi_threads.c
cp ./${src}/goi_omp.c ./temp/goi_omp.c

# Copy Other Files
cp ./${src}/sb/sb.h ./temp/sb/sb.h
cp ./${src}/sb/sb.c ./temp/sb/sb.c

cp ./${src}/util.h ./temp/util.h
cp ./${src}/util.c ./temp/util.c

cp ./${src}/exporter.h ./temp/exporter.h
cp ./${src}/exporter.c ./temp/exporter.c

cp ./${src}/settings.h ./temp/settings.h
cp ./${src}/main.c ./temp/main.c

# Copy Makefile & Test Cases
cp ./${src}/Makefile ./temp/Makefile
cp -r ./${src}/sample_inputs/ ./temp/testcases/

# ZIP Folders
cd ./temp
zip -r ./${studentid}.zip ./*
cd ../
mv ./temp/${studentid}.zip ./${studentid}.zip

# Delete Temporary Folders
rm -r temp

# Run Checker
./${src}/check_zip.sh ${studentid}.zip