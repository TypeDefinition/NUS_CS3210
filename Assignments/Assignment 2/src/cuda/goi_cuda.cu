#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <errno.h>

// including the "dead faction": 0
#define MAX_FACTIONS 10

// this macro is here to make the code slightly more readable, not because it can be safely changed to
// any integer value; changing this to a non-zero value may break the code
#define DEAD_FACTION 0

#define MAX_BLOCKS_PER_GRID 65535
#define MAX_THREADS_PER_BLOCK 1024

// Specifies the number(s) of live neighbors of the same faction required for a dead cell to become alive.
__device__ bool isBirthable(int n) { return n == 3; }

// Specifies the number(s) of live neighbors of the same faction required for a live cell to remain alive.
__device__ bool isSurvivable(int n) { return n == 2 || n == 3; }

// Specifies the number of live neighbors of a different faction required for a live cell to die due to fighting.
__device__ bool willFight(int n) { return n > 0; }

/**
 * Computes and returns the next state of the cell specified by row and col based on oldWorld and invaders. Sets *diedDueToFighting to
 * true if this cell should count towards the death toll due to fighting.
 * 
 * invaders can be NULL if there are no invaders.
 */
__device__ int getNextState(const int *oldWorld, const int *invaders, int nRows, int nCols, int row, int col, bool *diedDueToFighting)
{
    // we'll explicitly set if it was death due to fighting
    *diedDueToFighting = false;

    // faction of this cell
    int cell_faction = oldWorld[row * nCols + col];

    // did someone just get landed on?
    if (invaders != NULL && invaders[row * nCols + col] != DEAD_FACTION)
    {
        *diedDueToFighting = cell_faction != DEAD_FACTION;
        return invaders[row * nCols + col];
    }

    // tracks count of each faction adjacent to this cell
    int neighborCounts[MAX_FACTIONS];
    memset(neighborCounts, 0, MAX_FACTIONS * sizeof(int));

    // count neighbors (and self)
    for (int dy = -1; dy <= 1; dy++)
    {
        for (int dx = -1; dx <= 1; dx++)
        {
            int neighbourRow = row + dy;
            int neighbourCol = col + dx;
            if (neighbourRow < 0 || neighbourRow >= nRows || neighbourCol < 0 || neighbourCol >= nCols)
            {
                continue;
            }

            int faction = oldWorld[(row + dy) * nCols + (col + dx)];
            if (faction >= DEAD_FACTION)
            {
                neighborCounts[faction]++;
            }
        }
    }

    // we counted this cell as its "neighbor"; adjust for this
    neighborCounts[cell_faction]--;

    if (cell_faction == DEAD_FACTION)
    {
        // this is a dead cell; we need to see if a birth is possible:
        // need exactly 3 of a single faction; we don't care about other factions

        // by default, no birth
        int newFaction = DEAD_FACTION;

        // start at 1 because we ignore dead neighbors
        for (int faction = DEAD_FACTION + 1; faction < MAX_FACTIONS; faction++)
        {
            int count = neighborCounts[faction];
            if (isBirthable(count))
            {
                newFaction = faction;
            }
        }

        return newFaction;
    }
    else
    {
        /** 
         * this is a live cell; we follow the usual rules:
         * Death (fighting): > 0 hostile neighbor
         * Death (underpopulation): < 2 friendly neighbors and 0 hostile neighbors
         * Death (overpopulation): > 3 friendly neighbors and 0 hostile neighbors
         * Survival: 2 or 3 friendly neighbors and 0 hostile neighbors
         */

        int hostileCount = 0;
        for (int faction = DEAD_FACTION + 1; faction < MAX_FACTIONS; faction++)
        {
            if (faction == cell_faction)
            {
                continue;
            }
            hostileCount += neighborCounts[faction];
        }

        if (willFight(hostileCount))
        {
            *diedDueToFighting = true;
            return DEAD_FACTION;
        }

        int friendlyCount = neighborCounts[cell_faction];
        if (!isSurvivable(friendlyCount))
        {
            return DEAD_FACTION;
        }

        return cell_faction;
    }
}

__global__ void kernelFunc(int nRows, int nCols, int* oldWorld, int* newWorld, int *invasionPlan, int* deathTolls)
{
    // How many cells are there in the world?
    int totalCells = nRows * nCols;
    
    // How many threads are launched?
    int blocksPerGrid = (gridDim.x * gridDim.y * gridDim.z);
    int threadsPerBlock = (blockDim.x * blockDim.y * blockDim.z);
    int totalThreads = threadsPerBlock * blocksPerGrid;

    // Flatten the 3D index into a 1D index.
    int blockInGridID = (blockIdx.x + gridDim.x * (blockIdx.y + gridDim.y * blockIdx.z)); // The index of the block in the grid.
    int threadInBlockID = (threadIdx.x + blockDim.x * (threadIdx.y + blockDim.y * threadIdx.z)); // The index of the thread in the block.
    int threadInGridID = blockInGridID * threadsPerBlock + threadInBlockID; // The unique index of this thread amongst all the threads in the grid.

    // Shared memory buffer for faster access.
    __shared__ int sharedDeathTolls[MAX_THREADS_PER_BLOCK * 2]; // Double the size needed just in case MAX_THREADS_PER_BLOCK is wrong.
    if (threadInBlockID == 0) {
        memset((void*)sharedDeathTolls, 0, sizeof(sharedDeathTolls));
    }
    __syncthreads();

    // Allocate the work in a round-robin fashion. Global memory access is not cached anyways.
    int localDeathToll = 0;
    for (int cellIndex = threadInGridID; cellIndex < totalCells; cellIndex += totalThreads)
    {
        int row = cellIndex / nCols;
        int col = cellIndex % nCols;
        bool diedDueToFighting;
        int nextState = getNextState(oldWorld, invasionPlan, nRows, nCols, row, col, &diedDueToFighting);
        newWorld[row * nCols + col] = nextState;
        if (diedDueToFighting) { ++localDeathToll; }
    }

    sharedDeathTolls[threadInBlockID] = localDeathToll;
    __syncthreads();

    // Parallel Reduction
    int numWorkingThreads = threadsPerBlock / 2;
    while (numWorkingThreads > 0) {
        if (threadInBlockID < numWorkingThreads) {
            sharedDeathTolls[threadInBlockID] += sharedDeathTolls[threadInBlockID + numWorkingThreads];
        }
        numWorkingThreads /= 2;
        __syncthreads();
    }

    // Write to global memory.
    if (threadInBlockID == 0) {
        deathTolls[blockInGridID] += sharedDeathTolls[0];
    }
}

/**
 * The main simulation logic.
 * 
 * goi does not own startWorld, invasionTimes or invasionPlans and should not modify or attempt to free them.
 * nThreads is the number of threads to simulate with. It is ignored by the sequential implementation.
 */
__host__ int goi(dim3 gd, dim3 bd, int nGenerations, const int *startWorld, int nRows, int nCols, int nInvasions, const int *invasionTimes, int **invasionPlans)
{
    int totalCells = nRows * nCols;
    int blocksPerGrid = gd.x * gd.y * gd.z;

    // CPU memory.
    int* deathTolls = new int[blocksPerGrid];

    // GPU memory.
    int *oldWorldGPU, *newWorldGPU;
    int *invasionPlansGPU;
    int *deathTollsGPU;

    // Allocate GPU memory.
    cudaMalloc((void**)&oldWorldGPU, sizeof(int) * totalCells); // Allocate old world on GPU.
    cudaMalloc((void**)&newWorldGPU, sizeof(int) * totalCells); // Allocate new world on GPU.
    cudaMalloc((void**)&invasionPlansGPU, sizeof(int) * totalCells * nInvasions); // Allocate invasion plans on GPU.
    cudaMalloc((void**)&deathTollsGPU, sizeof(int) * blocksPerGrid); // Allocate an array for the death toll to be stored on the GPU.

    // Initialise GPU memory.
    cudaMemcpy((void*)oldWorldGPU, (void*)startWorld, sizeof(int) * totalCells, cudaMemcpyHostToDevice); // Copy start world from CPU to GPU.
    cudaMemset((void*)newWorldGPU, 0, sizeof(int) * totalCells);
    for (int i = 0; i < nInvasions; ++i) // Copy the invasion plans into a 1D array on the GPU.
    {
        cudaMemcpy((void*)&invasionPlansGPU[i * totalCells], (void*)invasionPlans[i], sizeof(int) * totalCells, cudaMemcpyHostToDevice);
    }
    cudaMemset((void*)deathTollsGPU, 0, sizeof(int) * blocksPerGrid);

    // Run simulation.
    int invasionIndex = 0;
    for (int i = 1; i <= nGenerations; ++i)
    {
        // Check for invasion.
        int* invasionPlanGPU = nullptr;
        if (invasionIndex < nInvasions && i == invasionTimes[invasionIndex]) {
            invasionPlanGPU = &invasionPlansGPU[invasionIndex * totalCells];
            ++invasionIndex;
        }

        kernelFunc<<<gd, bd>>>(nRows, nCols, oldWorldGPU, newWorldGPU, invasionPlanGPU, deathTollsGPU); // Launch kernel function.
        cudaDeviceSynchronize(); // Syncronise all threads in the grid.

        // Swap world buffers.
        int *temp = newWorldGPU;
        newWorldGPU = oldWorldGPU;
        oldWorldGPU = temp;
    }

    // Copy from GPU to CPU.
    cudaMemcpy((void*)deathTolls, (void*)deathTollsGPU, sizeof(int) * blocksPerGrid, cudaMemcpyDeviceToHost);

    // Add up all the deaths. (That's morbid...)
    int totalDeathToll = 0;
    for (int i = 0; i < blocksPerGrid; ++i) {
        if (i * blocksPerGrid >= totalCells) { break; } // These blocks did no work, so their death toll is for sure 0.
        totalDeathToll += deathTolls[i];
    }

    // Free allocated CPU memory.
    delete[] deathTolls;

    // Free allocated GPU memory.
    cudaFree((void*)newWorldGPU);
    cudaFree((void*)oldWorldGPU);
    cudaFree((void*)invasionPlansGPU);
    cudaFree((void*)deathTollsGPU);

    return totalDeathToll;
}